#!/bin/sh
# Portainer start script

# first time: docker volume create portainer_data

docker run -d -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data --name portainer -e VIRTUAL_HOST=some.name.local --network=le_ext --restart always portainer/portainer 