#!/bin/sh

docker run --detach \
    --name MyAPI --restart always \
    --volume /srv/docker/myapi/config:/etc/myapi/config \
    --volume /srv/docker/myapi/logs:/var/log/myapi \
    --volume /srv/docker/myapi/data:/var/opt/myapi \
    -e VIRTUAL_HOST=myapi.serverdomain.com
    --env "LETSENCRYPT_HOST=myapi.serverdomain.com" \
    --env "LETSENCRYPT_EMAIL=certificate-notification@myadmindomain.com" \
    dockergroup/containername:version