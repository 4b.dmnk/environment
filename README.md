# environment

As i've put some brainpower into "how to serve the API", with minimal attack surface and maintenance overhead; it deserves a project on it's own.  
It's a compilation of best-practices i've found during research, with some tuning.

## features
- reverse proxy: autoconfigured by the ENV Vars of the containers **jwilder/nginx-proxy**
- certificates: automatically aquired and renewed from lets-encrypt, based on ENV vars of the containers **jrcs/letsencrypt-nginx-proxy-companion**
- containers: automatically updated as soon as docker-hub has a newer release of the running version **containrrr/watchtower**
- web-based container management **portainer/portainer**
- example application configuration

## enhancements
- https://github.com/Einsteinish/Docker-compose-Nginx-Reverse-Proxy-II/tree/master/reverse-proxy

# first time

## photon os 3 (or any other OS that can run docker)
setup as described in your preferred guide, basically confirming everything till it's done ;)

after that:
 - tdnf is the package manager (never heard of that one before)
 - if you're on static IP's: configure network by creating /etc/systemd/network/10-static-en.network ... and `systemctl restart systemd-networkd`
 - enable & start docker `systemctl enable docker`, `systemctl start docker` and check if it worked by `docker info`

to enable absolute maintenance freeness: create a cronjob that runs `tdnf update --assumeyes`



> I've tried most of the setup inside a docker-windows host, the productive setup is running on a hardened Linux (VMWare Photon OS) 

## docker
subsequent starts will be handled by the docker daemon (e.g. after a system-reboot)

> change the vhost names in portainer and example_application, and either set your DNS accordingly or edit your local hosts file

```shell
cd project_directory
cd le
docker-compose up -d
cd
docker volume create portainer_data
./portainer.sh
./watchtower.sh
./example_application.sh
```
# basic wiring

diamonds are resources
dotted lines are internal calls

use [this link ](https://mermaidjs.github.io/mermaid-live-editor/#/view/eyJjb2RlIjoiZ3JhcGggTFJcbkFbQ2xpZW50XSAtLT58ODAvNDQzfCBDKG5naW54LXByb3h5KVxuICAgc3ViZ3JhcGggZG9ja2VyXG5oe2RvY2tlci5zb2NrfVxuXG5cbnN1YmdyYXBoIG1hbmFnZW1lbnRcbmggLS4tPldhdGNodG93ZXIoPGI-V2F0Y2h0b3dlcjwvYj48YnI-S2VlcGluZyBhbGwgQ29udGFpbmVycyA8YnI-dXAyZGF0ZSlcbldhdGNodG93ZXIgLS4tPmhcbmggLS4tPnkoPGI-UG9ydGFpbmVyPC9iPiA8YnI-IFdlYiBVSSBmb3IgRG9ja2VyKVxueSAtLi0-aFxuaC0uLT58XCJFTlY6IExFVFNFTkNSWVBUX0hPU1QgJiA8YnI-IEVOVjogTEVOVFNFTkNSWVBUX01BSUxcInxKKDxiPm5naW54LWxlLWNvbXBhbmlvbjwvYj4gPGJyPikgXG5KLS4tPiBJXG5DKG5naW54LXByb3h5KS0tPnw4MHxKXG5Je2NlcnRpZmljYXRlc30gLS4tPiBDXG5oIC0uLT58RU5WOlZJUlRVQUxfSE9TVCB8Q1xuQyAtLT58XCJtYW5hZ2VtZW50Lntob3N0bmFtZX0ubG9jYWxcInx5XG5lbmRcbkMgLS0-fFwiVklSVFVBTF9IT1NUICYgTEVUU0VOQ1JZUFRfSE9TVD0gPGJyPiBhcHBsaWNhdGlvbjEuZG9tYWluLmNvbSA8YnI-NDQzXCJ8IEcoXCJQcm9kdWN0aXZlIENvbnRhaW5lciA8YnI-VHlwbyAvIEdpdExhYiAvIFdvcmRwcmVzcyAvIC4uLlwiKVxuXG5DIC0tPnxcIlZJUlRVQUxfSE9TVCAmIExFVFNFTkNSWVBUX0hPU1Q9IDxicj4gYXBwbGljYXRpb24yLmRvbWFpbi5jb20gPGJyPjQ0M1wifCBNKC4uLilcbkMgLS0-fFwiVklSVFVBTF9IT1NUICYgTEVUU0VOQ1JZUFRfSE9TVD0gPGJyPiBhcHBsaWNhdGlvbjMuZG9tYWluLmNvbSA8YnI-NDQzXCJ8IEYobmdpbngtcHJveHkyKVxuZW5kXG5zdWJncmFwaCBleHRlcm5hbCB3ZWIgc2VydmljZXNcbldhdGNodG93ZXIgLS0-fGNoZWNrIGNvbnRhaW5lciB2ZXJzaW9uc3xEb2NrZXJodWJcbkogLS0-IHxmZXRjaCBjZXJ0aWZpY2F0ZXxMZXRzRW5jcnlwdFxuZW5kXG5zdWJncmFwaCBpbnRlcm5hbCB3ZWIgc2VydmljZXMsIGF1dG8tc2VydmVkIGNlcnRpZmljYXRlc1xuRiAtLT58YW5vdGhlciBzZXJ2ZXI8YnI-ODB8IE5bZmE6ZmEtY2FyIFR5cG8zXVxuZW5kXG4iLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9fQ) to get a better view, as long as GitLab is on an old meremaid renderer.
```mermaid
graph LR
A[Client] -->|80/443| C(nginx-proxy)
   subgraph docker
h{docker.sock}


subgraph management
h -.->Watchtower(<b>Watchtower</b><br>Keeping all Containers <br>up2date)
Watchtower -.->h
h -.->y(<b>Portainer</b> <br> Web UI for Docker)
y -.->h
h-.->|"ENV: LETSENCRYPT_HOST & <br> ENV: LENTSENCRYPT_MAIL"|J(<b>nginx-le-companion</b> <br>) 
J-.-> I
C(nginx-proxy)-->|80|J
I{certificates} -.-> C
h -.->|ENV:VIRTUAL_HOST |C
C -->|"management.{hostname}.local"|y
end
C -->|"VIRTUAL_HOST & LETSENCRYPT_HOST= <br> application1.domain.com <br>443"| G("Productive Container <br>Typo / GitLab / Wordpress / ...")

C -->|"VIRTUAL_HOST & LETSENCRYPT_HOST= <br> application2.domain.com <br>443"| M(...)
C -->|"VIRTUAL_HOST & LETSENCRYPT_HOST= <br> application3.domain.com <br>443"| F(nginx-proxy2)
end
subgraph external web services
Watchtower -->|check container versions|Dockerhub
J --> |fetch certificate|LetsEncrypt
end
subgraph internal web services, auto-served certificates
F -->|another server<br>80| N[fa:fa-car Typo3]
end
```
